<%--
  Created by IntelliJ IDEA.
  User: George
  Date: 5/14/2018
  Time: 2:16 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Notificari</title>
</head>
<body>
<b>Mai jos va puteti vedea toate notificarile primite asupra produselor dumneavoastra!</b>
<br>
<c:forTokens items = "${sell.notifications}" delims = "." var = "name">
<c:out value = "${name}"/><p>
    </c:forTokens>
<br>
<form action="/shownotifications/seller" method="GET">
    <input name="Back" type="submit" value="Back">
</form>
</body>
</html>
