<%--
  Created by IntelliJ IDEA.
  User: Denisa
  Date: 5/5/2018
  Time: 11:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Buy products</title>
</head>
<body>
Mai jos aveti toate produsele acestui seller!
<br>
<c:forEach items="${sellerProducts}" var="products" varStatus="status">
    <br>
    <b>Categorie: </b>
    <c:out value="${products.category}"/>
    <br>
    <b>Numele: </b>
    <c:out value="${products.name}"/>
    <br>
    <b>Pretul: </b>
    <c:out value="${products.price}"/>
    <br>
    <b>Stock-ul ramas: </b>
    <c:out value="${stocks[status.index]}"/>
    <br>
</c:forEach>
<br>
<b><i>
    Alegeti produsul pe care doriti sa-l cumparati!
</i></b>

<p><span style = "color:red"> ${errorMessage}</span></p>
<form action="/buyproducts/buyproducts" method="POST">
    Numele produsului: <input name="productName" type="text"/>
    Cantitatea: <input name="quantity" type="text"/>
    <input name="Add to cart!"  type="submit" value="Add to cart!"/>
</form>

<br>
<form action="/buyproducts/buyer" method="GET">
    <input name="Back" type="submit" value="Back">
</form>

</body>
</html>
