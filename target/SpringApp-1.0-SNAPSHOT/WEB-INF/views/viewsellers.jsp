<%--
  Created by IntelliJ IDEA.
  User: Denisa
  Date: 5/2/2018
  Time: 12:07 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Select seller!</title>
</head>
<body>
<p>Alegeti vanzatorul!</p>

<c:forEach items="${sell}" var="seller">
    <form action="/viewsellers/viewsellers" method="POST">
        <input name="selectseller" type="submit" value="${seller.username}"/>
    </form>
</c:forEach>

<br>
<form action="/viewsellers/buyer" method="GET">
    <input name="Back" type="submit" value="Back"/>
</form>
</body>
</html>
