<%--
  Created by IntelliJ IDEA.
  User: George
  Date: 5/14/2018
  Time: 11:53 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Add products</title>
</head>
<body>
<p>Mai jos, poti alege dintre produsele prezentate pentru a le adauga in stock-ul tau! </br> </p>
<p><span style = "color:red"> ${errorMessage}</span></p>
<c:forEach items="${productsToAdd}" var="products">
    <br>
    <b>Categoria: </b>
    <c:out value="${products.category}"/>
    <br>
    <b>Numele produsului: </b>
    <c:out value="${products.name}"/>
    <br>
    <b>Pretul produsului: </b>
    <c:out value="${products.price}"/>
    <br>
</c:forEach>
<br>

<form action="/addproducts/addproducts" method="POST">
    Numele produsului: <input name="productName" type="text"/>
    Cantitatea dorita: <input name="quantity" type="text"/>
    <input name="Add!"  type="submit" value="Add!"/>
</form>

<form action="/addproducts/seller" method="GET">
    <input name="Back" type="submit" value="Back">
</form>

</body>
</html>
