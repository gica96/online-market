package com.in28minutes.bl;

import com.in28minutes.dao.BuyerDAO;
import com.in28minutes.dao.BuyerIDAO;
import com.in28minutes.model.Buyer;

import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BuyerBL {
    
    private BuyerIDAO buyerDao;
    
    public BuyerBL()
    {
        this.buyerDao = new BuyerDAO();
    }
    
    public Buyer findBuyerByUsernameAndPassword(String username, String password)
    {
        return buyerDao.findBuyerByUsernameAndPassword(username, password);
    }
    
    public void loadOrders(Buyer buyer)
    {
        buyerDao.loadOrders(buyer);
    }
    
    public Product getProductByOrder(MyOrder order)
    {
        return buyerDao.getProductByOrder(order);
    }
    
    public List<Seller> getAllSellers()
    {
        return buyerDao.getAllSellers();
    }
    
    public Seller getSellerByUsername(String username)
    {
        return buyerDao.findSellerByUsername(username);
    }
    
    public List<Product> getProductsBySeller(Seller seller)
    {
        return buyerDao.getProductsBySeller(seller);
    }
    
    public int getProductStock(Product product)
    {
        return buyerDao.getProductStock(product);
    }
    
    public Product getProductByName(String name)
    {
        return buyerDao.getProductByName(name);
    }
    
    public void updateProductStock(Product product, int stock)
    {
        buyerDao.updateProductStock(product, stock);
    }
    
    public void addToCart(MyOrder order)
    {
        buyerDao.addToCart(order);
    }
    
    public void setIsPayed(MyOrder order)
    {
        buyerDao.setIsPayed(order);
    }
    
    public void updateSellerMoney(int sellerId, double sum)
    {
        buyerDao.updateSellerMoney(sellerId, sum);
    }
    
    public void updateBuyerMoney(Buyer buyer, double sum)
    {
        buyerDao.updateBuyerMoney(buyer, sum);
    }
    
    public Seller getSellerById(int id)
    {
        return buyerDao.findSellerById(id);
    }
    
    public void updateNotifications(Seller seller, String newNotification)
    {
        buyerDao.updateSellerNotifications(seller, newNotification); 
    }
}
