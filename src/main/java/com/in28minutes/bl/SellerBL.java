package com.in28minutes.bl;


import com.in28minutes.dao.SellerDAO;
import com.in28minutes.dao.SellerIDAO;
import com.in28minutes.model.Product;
import com.in28minutes.model.ProductStock;
import com.in28minutes.model.Seller;
import org.springframework.stereotype.Service;

@Service
public class SellerBL {

    private SellerIDAO sellerDao;

    public SellerBL()
    {
        this.sellerDao = new SellerDAO();
    }
    
    public Seller getSellerByUsernameAndPassword(String username, String password)
    {
        return sellerDao.findSellerByUsernameAndPassword(username, password);
    }
    
    public void loadProducts(Seller seller)
    {
        sellerDao.loadProducts(seller);
    }
    
    public int getProductStock(Product product)
    {
        return sellerDao.getProductStock(product);
    }
    
    public Product getProductByName(String name)
    {
        return sellerDao.getProductByName(name);
    }
    
    public void updateProductStock(Product product, int newStock)
    {
        sellerDao.updateProductStock(product, newStock);
    }
    
    public void updateProductPrice(Product product, double newPrice)
    {
        sellerDao.updateProductPrice(product, newPrice);
    }
    
    public void insertProduct(Product product)
    {
        sellerDao.insertProduct(product);
    }
    
    public void insertProductStock(ProductStock productStock)
    {
        sellerDao.insertProductStock(productStock);
    }
    
    public void updateNotifications(Product product, String notification)
    {
        
    }
}
