package com.in28minutes.strategy;

import com.in28minutes.model.Product;

import java.util.List;

public interface IComparator {
    
    void compareProducts(List<Product> products);
}
