package com.in28minutes.strategy;

import com.in28minutes.model.Product;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class AscendingComparator implements IComparator {


    public void compareProducts(List<Product> products) 
    {
        Collections.sort(products, new Comparator<Product>() {
            
            @Override
            public int compare(Product p1, Product p2)
            {
                return p1.getPrice() > p2.getPrice() ? 1 : (p1.getPrice() < p2.getPrice()) ? -1 : 0;
            }
            
        });
    }
}
