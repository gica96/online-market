package com.in28minutes.strategy;

public class Creator {
    
    public IComparator chooseStrategy(int number)
    {
        if(number == 0)
        {
            return new AscendingComparator();
        }
        else if(number == 1)
        {
            return new DescendingComparator();
        }
        else 
        {
            return null;
        }
    }
}
