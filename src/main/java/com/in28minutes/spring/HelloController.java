package com.in28minutes.spring;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {
    
    @RequestMapping(value = "/hello")
    public String helloWorld()
    {
        return "helloworld";
    }
}
