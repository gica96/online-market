package com.in28minutes.spring;

import com.in28minutes.jee.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

//tell Spring that this is a controller
@Controller
public class LoginController {
    
    //auto wiring -> take the bean and put it here (or where it matches)
    @Autowired
    LoginService loginService;
    
    @RequestMapping(value="/login", method = RequestMethod.GET)
    public String showLoginPage()
    {
        return "login";
    }
    
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String handleRequest(@RequestParam String name, @RequestParam String password, ModelMap modelMap)
    {
        if(loginService.validateUser(name, password))
        {
            modelMap.put("name", name);
            modelMap.put("password", password);
            return "helloworld";
        }
        else 
        {
            modelMap.put("errorMessage", "Invalid username or password!");
            return "login";
        }
    }
}