package com.in28minutes.decorator;


public class Decorator implements IDecorator {

    private IDecorator decorator;
    private double someAmount;
    
    public Decorator(IDecorator decorator, double someAmount)
    {
        this.decorator = decorator;
        this.someAmount = someAmount;
    }
    
    public double operation() 
    {
        double priceAfterOperation = decorator.operation();
        double moreReducedPrice = priceAfterOperation - (someAmount/100) * priceAfterOperation;
        return moreReducedPrice;
    }
}
