package com.in28minutes.decorator;

public interface IDecorator {
    
    double operation();
}
