package com.in28minutes.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "Buyer")
public class Buyer {

    @Id
    @Column(name = "idBuyer")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idBuyer;
    
    @Column(name = "username")
    private String username;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "money")
    private double money;
    
    @OneToMany
    @JoinColumn(name = "idBuyer_order")
    private List<MyOrder> orders;

    public Buyer()
    {
        //
    }
    
    public void renewOrders()
    {
        this.orders = new ArrayList<MyOrder>();
    }
    
    public void setOrders(List<MyOrder> orders)
    {
        this.orders = orders;
    }
    
    public void addOrder(MyOrder order)
    {
        this.orders.add(order);
    }
    
    public Buyer(String username, String password)
    {
        this.username = username;
        this.password = password;
        this.orders = new ArrayList<MyOrder>();
    }

    public List<MyOrder> getOrders() {
        return orders;
    }

    public int getIdBuyer() {
        return idBuyer;
    }

    public void setIdBuyer(int idBuyer) {
        this.idBuyer = idBuyer;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
