package com.in28minutes.model;

import javax.persistence.*;

@Entity
@Table (name = "ProductStock")
public class ProductStock {

    @Id
    @Column(name = "idProductStock")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idProductStock;
    
    @Column(name = "stock")
    private int stock;
    
    @Column(name = "idProduct_stock")
    private int idProduct;
    
    public ProductStock()
    {
        //
    }
    
    public ProductStock(int stock, int idProduct)
    {
        this.stock = stock;
        this.idProduct = idProduct;
    }

    public int getIdProductStock() {
        return idProductStock;
    }

    public int getStock() {
        return stock;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }
}
