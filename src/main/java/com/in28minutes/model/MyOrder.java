package com.in28minutes.model;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "MyOrder")
public class MyOrder {

    @Id
    @Column(name = "idOrder")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idOrder;
    
    @Column(name = "bill_number")
    private String billNumber;
    
    @Column(name = "date")
    private Date date;
    
    @Column(name = "quantity")
    private int quantity;
    
    @Column(name = "isPayed")
    private int isPayed;
    
    @Column(name = "idBuyer_order")
    private int idBuyer;

    @Column (name = "idProduct_order")
    private int idProduct;

    public MyOrder(String billNumber, Date date, int quantity, int isPayed, int idBuyer, int idProduct) {
        this.billNumber = billNumber;
        this.date = date;
        this.quantity = quantity;
        this.isPayed = isPayed;
        this.idBuyer = idBuyer;
        this.idProduct = idProduct;
    }
    
    public MyOrder()
    {
        //
    }

    public void setIsPayed(int isPayed) {
        this.isPayed = isPayed;
    }

    public String getBillNumber() {
        return billNumber;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public int getIsPayed() {
        return isPayed;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    
    public int getIdOrder() {
        return idOrder;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getIdBuyer() {
        return idBuyer;
    }

    public void setIdBuyer(int idBuyer) {
        this.idBuyer = idBuyer;
    }
}
