package com.in28minutes.model;

import com.in28minutes.IObserver.IObserver;
import com.in28minutes.bl.BuyerBL;
import com.in28minutes.bl.SellerBL;

import javax.persistence.*;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.*;

@Entity
@Table(name = "Seller")
public class Seller implements IObserver {

    @Id
    @Column(name = "idSeller")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idSeller;
    
    @Column(name = "username")
    private String username;
    
    @Column(name = "password")
    private String password;
    
    @Column(name = "notifications")
    private String notifications;
    
    @Column(name = "money")
    private double money;
    
    @OneToMany
    @JoinColumn(name = "idSeller_product")
    private List<Product> products;
    
    public Seller()
    {
        
    }

    public String getNotifications() {
        return notifications;
    }
    
    public void setNotifications(String s)
    {
        this.notifications = s;
    }
    
    public void addProduct(Product product)
    {
        this.products.add(product);
    }
    
    public void renewProducts()
    {
        this.products = new ArrayList<Product>();
    }

    public List<Product> getProducts() {
        return products;
    }

    public int getIdSeller() {
        return idSeller;
    }

    public void setIdSeller(int idSeller) {
        this.idSeller = idSeller;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public void update(Product product, Buyer buyer, int quantity, BuyerBL buyerBL) 
    {
        
        String toAdd = "Produsul: " + product.getName() + " din categoria: " + product.getCategory() + 
                " a fost cumparat de catre: " + buyer.getUsername() + " in cantitate de: " + quantity + " bucata/bucati.";
        
        buyerBL.updateNotifications(this, toAdd);
        
    }
}