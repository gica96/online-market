package com.in28minutes.model;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.decorator.IDecorator;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "Product")
public class Product implements IDecorator {

    @Id
    @Column(name = "idProduct")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idProduct;
    
    @Column(name = "category")
    private String category;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "price")
    private double price;
    
    @Column(name = "idSeller_product")
    private int idSeller;
    
    @OneToOne(cascade = CascadeType.ALL)
    @PrimaryKeyJoinColumn
    private ProductStock productStock;
    
    @OneToMany
    @JoinColumn(name = "idProduct_order")
    private List<MyOrder> orders;
    
    public Product()
    {
        //
    }

    public void setIdSeller(int idSeller) {
        this.idSeller = idSeller;
    }

    public Product(String category, String name, double price) {
        this.category = category;
        this.name = name;
        this.price = price;
    }

    public ProductStock getProductStock() {
        return productStock;
    }

    public List<MyOrder> getOrders() {
        return orders;
    }

    public int getIdProduct()
    {
        return this.idProduct;
    }
    
    public String getCategory()
    {
        return this.category;
    }
    
    public String getName()
    {
        return this.name;
    }

    public double getPrice() {
        return price;
    }

    public int getIdSeller() {
        return idSeller;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public double operation()
    {
        double price = this.getPrice();
        double newPrice = price - ((double)10/100) * price;
        this.setPrice(newPrice);
        return newPrice;
    }
    
    public void notifyObserver(Seller seller, Buyer buyer, int quantity, BuyerBL buyerBL)
    {
        seller.update(this, buyer, quantity, buyerBL);
    }
}
