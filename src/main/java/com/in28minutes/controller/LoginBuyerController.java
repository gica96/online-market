package com.in28minutes.controller;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.model.Buyer;
import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
@SessionAttributes({"buy", "products", "billnumber"})
public class LoginBuyerController {
    
    @Autowired
    BuyerBL buyerBL;

    @RequestMapping(value="/loginbuyer", method = RequestMethod.GET)
    public String showLoginPage(ModelMap modelMap)
    {
        List<Product> products = new ArrayList<Product>();
        Buyer buyer = new Buyer();
        modelMap.addAttribute("buy",buyer);
        modelMap.addAttribute("products", products);
        return "loginbuyer";
    }
    
    @RequestMapping(value = "/loginbuyer", method = RequestMethod.POST)
    public String handleRequest(@ModelAttribute("buy") Buyer buyer, 
                                @ModelAttribute("products") ArrayList<Product> products,
                                @RequestParam String name, 
                                @RequestParam String password, 
                                ModelMap modelMap)
    {
        Buyer newBuyer = buyerBL.findBuyerByUsernameAndPassword(name, password);
        if(newBuyer != null)
        {
            buyer.setIdBuyer(newBuyer.getIdBuyer());
            buyer.setMoney(newBuyer.getMoney());
            buyer.setUsername(newBuyer.getUsername());
            buyer.setPassword(newBuyer.getPassword());
            
            buyerBL.loadOrders(newBuyer);
            buyer.setOrders(newBuyer.getOrders());

            for(MyOrder order : buyer.getOrders())
            {
                products.add(buyerBL.getProductByOrder(order));
            }

            Random random = new Random();
            int low = 10000;
            int high = 99999;
            int randomNumber = random.nextInt(high-low) + low;
            
            modelMap.addAttribute("billnumber", randomNumber);
            modelMap.addAttribute("buy", buyer);
            modelMap.addAttribute("products", products);
            return "buyer";
        }
        else 
        {
            modelMap.put("errorMessage", "Acest buyer nu exista!");
            return "loginbuyer";
        }
    }
}
