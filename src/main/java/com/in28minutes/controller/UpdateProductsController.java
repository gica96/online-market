package com.in28minutes.controller;

import com.in28minutes.bl.SellerBL;
import com.in28minutes.decorator.Decorator;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@Controller 
@SessionAttributes({"sell", "stocks"})
@RequestMapping("/updateproducts")
public class UpdateProductsController {
    
    @Autowired
    SellerBL sellerBL;

    @RequestMapping(value="/updateproducts", method = RequestMethod.GET)
    public String showUpdatePageSeller(@ModelAttribute("sell") Seller seller,
                                 @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                 ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", stocks);
        return "updateproducts";
    }
    
    
    @RequestMapping(value = "/updateproducts", params = {"Set stock!"}, method = RequestMethod.POST)
    public String handleSetStock(@ModelAttribute("sell") Seller seller,
                                 @RequestParam String productName,
                                 @RequestParam String quantity,
                                 ModelMap modelMap)
    {
        boolean ok = false;
        Product selectedProduct = sellerBL.getProductByName(productName);
        ArrayList<Integer> updatedStocks = new ArrayList<Integer>();
        if(selectedProduct != null)
        {
            for(Product product : seller.getProducts())
            {
                if(product.getName().equals(selectedProduct.getName()))
                {
                    ok = true;
                    break;
                }
            }
            if(ok)
            {
                sellerBL.updateProductStock(selectedProduct, Integer.parseInt(quantity));
                for(Product product : seller.getProducts())
                {
                    updatedStocks.add(sellerBL.getProductStock(product));
                }
                modelMap.addAttribute("sell", seller);
                modelMap.addAttribute("stocks", updatedStocks);
                return "updateproducts";
            }
            else 
            {
                modelMap.put("errorMessage", "Acest produs nu va apartine!");
                return "updateproducts";
            }
            
        }
        else 
        {
            modelMap.put("errorMessage", "Acest produs nu exista!");
            return "updateproducts";
        }
    }


    @RequestMapping(value = "/updateproducts", params = {"Do discount!"}, method = RequestMethod.POST)
    public String handleDoDiscount(@ModelAttribute("sell") Seller seller,
                                 @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                 @RequestParam String productName,
                                 ModelMap modelMap)
    {
        boolean ok = false;
        Product selectedProduct = sellerBL.getProductByName(productName);
        if(selectedProduct != null)
        {
            for(Product product : seller.getProducts())
            {
                if(product.getName().equals(selectedProduct.getName()))
                {
                    ok = true;
                    break;
                }
            }
            if(ok)
            {
                Decorator decorator = new Decorator(selectedProduct, 10.0);
                double afterDiscountPrice = decorator.operation();
                sellerBL.updateProductPrice(selectedProduct, afterDiscountPrice);
                sellerBL.loadProducts(seller);

                modelMap.addAttribute("sell", seller);
                modelMap.addAttribute("stocks", stocks);
                return "updateproducts";
            }
            else 
            {
                modelMap.put("errorMessage", "Acest produs nu va apartine!");
                return "updateproducts";
            }
            
        }
        else
        {
            modelMap.put("errorMessage", "Acest produs nu exista!");
            return "updateproducts";
        }
    }
    
    
    @RequestMapping(value = "/seller", params = {"Back"}, method = RequestMethod.GET)
    public String handleBackButtonUpdate(@ModelAttribute("sell") Seller seller,
                                         @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                         ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", stocks);
        return "seller";
    }
}
