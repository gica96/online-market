package com.in28minutes.controller;

import com.in28minutes.bl.SellerBL;
import com.in28minutes.model.Product;
import com.in28minutes.model.ProductStock;
import com.in28minutes.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import javax.enterprise.inject.Model;
import java.util.ArrayList;

@Controller
@SessionAttributes({"sell", "productsToAdd"})
@RequestMapping("/addproducts")
public class AddProductsController {
    
    @Autowired
    SellerBL sellerBL;
    
    @RequestMapping(value = "/addproducts", method = RequestMethod.GET)
    public String showAddProductPageSeller(@ModelAttribute("sell")Seller seller,
                                           @ModelAttribute("productsToAdd")ArrayList<Product> productsToAdd,
                                           ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("productsToAdd", productsToAdd);
        return "addproducts";
    }
    
    @RequestMapping(value = "/addproducts", method = RequestMethod.POST)
    public String handleAddProduct(@ModelAttribute("sell") Seller seller,
                                   @ModelAttribute("productsToAdd") ArrayList<Product> productsToAdd,
                                   @RequestParam String productName,
                                   @RequestParam String quantity,
                                   ModelMap modelMap)
    {
        Product chosenProduct = null;
        boolean ok = false;
        for(Product product : productsToAdd)
        {
            if(product.getName().equals(productName))
            {
                ok = true;
                chosenProduct = product;
                break;
            }
        }
        if(!ok)
        {
            modelMap.put("errorMessage", "Puteti selecta doar din lista de mai jos!");
            return "addproducts";
        }
        else 
        {   
            if(Integer.parseInt(quantity) <= 0)
            {
                modelMap.put("errorMessage", "Cantitate invalida!");
                return "addproducts";
            }
            else 
            {
                for(Product product : seller.getProducts())
                {
                    if(product.getName().equals(productName))
                    {
                        modelMap.put("errorMessage", "Aveti deja pe stock acest produs! Accesati pagina de Update Products pt. mai multe detalii!");
                        return "addproducts";
                    }
                }
                chosenProduct.setIdSeller(seller.getIdSeller());
                sellerBL.insertProduct(chosenProduct);
                chosenProduct = sellerBL.getProductByName(productName);

                ProductStock productStock = new ProductStock(Integer.parseInt(quantity), chosenProduct.getIdProduct());
                sellerBL.insertProductStock(productStock);
                sellerBL.loadProducts(seller);
                return "addproducts";
            }
        }
    }
    
    @RequestMapping(value = "/seller", params = {"Back"}, method = RequestMethod.GET)
    public String handleBackButton(@ModelAttribute("sell") Seller seller,
                                   ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        return "seller";
    }
}
