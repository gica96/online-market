package com.in28minutes.controller;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.model.Buyer;
import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes({"cartorders", "cartproducts", "buy", "billnumber"})
@RequestMapping("/viewcart")
public class ViewCartController {
    
    @Autowired
    BuyerBL buyerBL;

    @RequestMapping(value="/viewcart", method = RequestMethod.GET)
    public String showCartPage(@ModelAttribute("cartproducts") ArrayList<Product> products, 
                               @ModelAttribute("cartorders") ArrayList<MyOrder> orders, 
                               @ModelAttribute("buy") Buyer buyer,
                               @ModelAttribute("billnumber") Integer randomNumber,
                               ModelMap modelMap)
    {
        modelMap.addAttribute("cartproducts", products);
        modelMap.addAttribute("cartorders",orders);
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("billnumber", randomNumber);
        return "viewcart";
    }
    
    @RequestMapping(value = "/viewcart", method = RequestMethod.POST)
    public String handlePaying(@ModelAttribute("cartproducts") ArrayList<Product> products,
                               @ModelAttribute("cartorders") ArrayList<MyOrder> orders,
                               @ModelAttribute("buy") Buyer buyer,
                               @ModelAttribute("billnumber") Integer randomNumber,
                               @RequestParam String billNumber,
                               ModelMap modelMap)        
    {
        boolean existsBill = false;
        for(MyOrder order : orders)
        {
            if(order.getBillNumber().equals(billNumber))
            {
                existsBill = true;
                break;
            }
        }
        if(!existsBill)
        {
            modelMap.put("errorMessage", "Numarul facturii este invalid!");
            return "viewcart";
        }

        List<MyOrder> bills = new ArrayList<MyOrder>();
        for(MyOrder order : orders)
        {
            if(order.getBillNumber().equals(billNumber))
            {
                bills.add(order);
            }
        }
        
        for(MyOrder order : bills)
        {
            buyerBL.setIsPayed(order);
        }

        double sumForSellectedBill = 0;
        for(MyOrder order : bills)
        {
            Product product = buyerBL.getProductByOrder(order);
            double priceForSeller = order.getQuantity() * product.getPrice();
            int sellerId = product.getIdSeller();
            Seller seller = buyerBL.getSellerById(sellerId);
            product.notifyObserver(seller, buyer, order.getQuantity(), buyerBL);
            buyerBL.updateSellerMoney(sellerId, priceForSeller);
            sumForSellectedBill += priceForSeller;
        }
        
        if(sumForSellectedBill > 0)
        {
            if(sumForSellectedBill > buyer.getMoney())
            {
                modelMap.put("errorMessage", "Nu aveti suficienti bani!");
                return "viewcart";
            }
        }
        
        buyerBL.updateBuyerMoney(buyer, sumForSellectedBill);
        buyer.setMoney(buyer.getMoney() - sumForSellectedBill);

        buyerBL.loadOrders(buyer);
        List<MyOrder> newOrders = new ArrayList<MyOrder>();
        for(MyOrder order : buyer.getOrders())
        {
            if(order.getIsPayed() == 0)
            {
                newOrders.add(order);
            }
        }

        List<Product> newProducts = new ArrayList<Product>();
        for(MyOrder order : newOrders)
        {
            newProducts.add(buyerBL.getProductByOrder(order));
        }

        modelMap.addAttribute("cartorders", newOrders);
        modelMap.addAttribute("cartproducts", newProducts);
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("billnumber", randomNumber);
        return "viewcart";
    }

    @RequestMapping(value = "/buyer", params={"Back"}, method = RequestMethod.POST)
    public String handleBackButtonCart(@ModelAttribute("buy") Buyer buyer,
                                       @ModelAttribute("billnumber") Integer randomNumber,
                                       ModelMap modelMap)
    {
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("billnumber", randomNumber);
        return "buyer";
    }
}
