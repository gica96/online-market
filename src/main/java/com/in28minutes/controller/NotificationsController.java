package com.in28minutes.controller;

import com.in28minutes.model.Seller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

@Controller
@SessionAttributes({"sell"})
@RequestMapping("/shownotifications")
public class NotificationsController {
    
    @RequestMapping(value = "/shownotifications", method = RequestMethod.GET)
    public String showNotificationsPage(@ModelAttribute("sell")Seller seller,
                                        ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        return "shownotifications";
    }

    @RequestMapping(value = "/seller", params={"Back"}, method = RequestMethod.GET)
    public String handleBackButtonNotifications(@ModelAttribute("sell") Seller seller,
                                          ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        return "seller";
    }
}
