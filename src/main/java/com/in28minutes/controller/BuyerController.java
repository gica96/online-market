package com.in28minutes.controller;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.model.Buyer;
import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;


@Controller
@SessionAttributes({"buy", 
                    "products", 
                    "payedorders", 
                    "payedproducts", 
                    "cartorders", 
                    "cartproducts",
                    "billnumber",
                    "sell"})
public class BuyerController {
    
    @Autowired
    BuyerBL buyerBL;

    @RequestMapping(value="/buyer", method = RequestMethod.GET)
    public String showBuyerPage(@ModelAttribute ("products") ArrayList<Product> products, 
                                @ModelAttribute("buy") Buyer buyer,
                                @ModelAttribute("billnumber") Integer randomNumber,
                                ModelMap modelMap)
    {
        modelMap.addAttribute("buy",buyer);
        modelMap.addAttribute("products", products);
        modelMap.addAttribute("billnumber", randomNumber);
        return "buyer";
    }
    
    @RequestMapping(value = "/buyer", params={"View orders!"}, method = RequestMethod.GET)
    public String handleViewOrders(@ModelAttribute("buy") Buyer buyer,
                                   ModelMap modelMap)
    {
        buyerBL.loadOrders(buyer);
        List<MyOrder> newOrders = new ArrayList<MyOrder>();
        for(MyOrder order : buyer.getOrders())
        {
            if(order.getIsPayed() == 1)
            {
                newOrders.add(order);
            }
        }
        
        List<Product> newProducts = new ArrayList<Product>();
        for(MyOrder order : newOrders)
        {
            newProducts.add(buyerBL.getProductByOrder(order));
        }
        
        modelMap.addAttribute("payedorders", newOrders);
        modelMap.addAttribute("payedproducts", newProducts);
        return "vieworders";
    }

    @RequestMapping(value = "/buyer", params={"View cart!"}, method = RequestMethod.GET)
    public String handleViewCart(@ModelAttribute("buy") Buyer buyer, 
                                 @ModelAttribute("billnumber") Integer randomNumber,
                                 ModelMap modelMap)
    {
        buyerBL.loadOrders(buyer);
        List<MyOrder> newOrders = new ArrayList<MyOrder>();
        for(MyOrder order : buyer.getOrders())
        {
            if(order.getIsPayed() == 0)
            {
                newOrders.add(order);
            }
        }

        List<Product> newProducts = new ArrayList<Product>();
        for(MyOrder order : newOrders)
        {
            newProducts.add(buyerBL.getProductByOrder(order));
        }

        modelMap.addAttribute("cartorders", newOrders);
        modelMap.addAttribute("cartproducts", newProducts);
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("billnumber", randomNumber);
        
        return "viewcart";
    }
    
    @RequestMapping(value = "/buyer", params = {"Select seller!"}, method = RequestMethod.GET)
    public String handleViewSellers(@ModelAttribute("buy") Buyer buyer, 
                                    @ModelAttribute("billnumber") Integer randomNumber,
                                    ModelMap modelMap)
    {
        List<Seller> sellers;
        sellers = buyerBL.getAllSellers();

        modelMap.addAttribute("sell", sellers);
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("buy",buyer);
        return "viewsellers";
    }
}
