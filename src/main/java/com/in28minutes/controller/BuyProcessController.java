package com.in28minutes.controller;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.model.Buyer;
import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Controller
@SessionAttributes({"billnumber", "buy", "sellerProducts", "stocks"})
@RequestMapping("/buyproducts")
public class BuyProcessController {
    
    @Autowired
    BuyerBL buyerBL;

    @RequestMapping(value="/buyproducts", method = RequestMethod.GET)
    public String showSellersPage(@ModelAttribute("billnumber") Integer randomNumber,
                                  @ModelAttribute("buy") Buyer buyer,
                                  @ModelAttribute("sellerProducts") ArrayList<Product> sellersProducts,
                                  @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                  ModelMap modelMap)
    {
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("sellerProducts", sellersProducts);
        modelMap.addAttribute("stocks", stocks);
        return "buyproducts";
    }
    
    @RequestMapping(value = "/buyproducts", method = RequestMethod.POST)
    public String handleTheBuying(@ModelAttribute("billnumber") Integer randomNumber,
                                  @ModelAttribute("buy") Buyer buyer,
                                  @ModelAttribute("sellerProducts") ArrayList<Product> sellersProducts,
                                  @RequestParam String productName,
                                  @RequestParam String quantity,
                                  ModelMap modelMap)
    {
        Product boughtProduct = buyerBL.getProductByName(productName);
        boolean exists = false;
        if(boughtProduct == null)
        {
            modelMap.put("errorMessage", "Acest produs nu exista!");
            return "buyproducts";
        }
        else
        {
            for(Product product : sellersProducts)
            {
                if(product.getName().equals(boughtProduct.getName()))
                {
                    exists = true;
                }
            }
            if(!exists)
            {
                modelMap.put("errorMessage", "Acest seller nu detine acest produs!");
                return "buyproducts";
            }
        }
        
        int productStock = buyerBL.getProductStock(boughtProduct);
        if(Integer.parseInt(quantity) > productStock)
        {
            modelMap.put("errorMessage", "Nu exista atata cantitate pe stock!");
            return "buyproducts";
        }

        Calendar currenttime = Calendar.getInstance();
        Date sqldate = new Date((currenttime.getTime()).getTime());
        MyOrder myOrder = new MyOrder(String.valueOf(randomNumber),
                                      sqldate, Integer.parseInt(quantity),
                                      0, buyer.getIdBuyer(),
                                      boughtProduct.getIdProduct());
        
        int newStock = productStock - Integer.parseInt(quantity);
        List<Integer> stocks = new ArrayList<Integer>();

        buyerBL.updateProductStock(boughtProduct, newStock);
        for(Product product : sellersProducts)
        {
            stocks.add(buyerBL.getProductStock(product));
        }
        
        buyerBL.addToCart(myOrder);
        
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("sellerProducts", sellersProducts);
        modelMap.addAttribute("stocks", stocks);
        
        return "buyproducts";
    }

    @RequestMapping(value = "/buyer", params={"Back"}, method = RequestMethod.GET)
    public String handleBackButtonBuy(@ModelAttribute("buy") Buyer buyer,
                                      @ModelAttribute("billnumber") Integer randomNumber,
                                      ModelMap modelMap)
    {
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("buy", buyer);
        return "buyer";
    }
}
