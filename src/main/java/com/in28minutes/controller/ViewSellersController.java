package com.in28minutes.controller;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.model.Buyer;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes({"sell", "billnumber", "buy", "sellerProducts", "stocks"})
@RequestMapping("/viewsellers")
public class ViewSellersController {
    
    @Autowired
    BuyerBL buyerBL;

    @RequestMapping(value="/viewsellers", method = RequestMethod.GET)
    public String showSellersPage(@ModelAttribute("billnumber") Integer randomNumber,
                                  @ModelAttribute("sell") ArrayList<Seller> sellers,
                                  @ModelAttribute("buy") Buyer buyer,
                                  ModelMap modelMap)
    {
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("sell", sellers);
        modelMap.addAttribute("buy", buyer);
        return "viewsellers";
    }



    @RequestMapping(value="/viewsellers", method = RequestMethod.POST)
    public String handleSelection(@ModelAttribute("billnumber") Integer randomNumber,
                                  @ModelAttribute("sell") ArrayList<Seller> sellers,
                                  @ModelAttribute("buy") Buyer buyer,
                                  @RequestParam(value = "selectseller") String sellerUsername,
                                  ModelMap modelMap)
    {
        Seller sellectedSeller = buyerBL.getSellerByUsername(sellerUsername);
        List<Product> sellersProducts = buyerBL.getProductsBySeller(sellectedSeller);
        List<Integer> stocks = new ArrayList<Integer>();
        
        for(Product product : sellersProducts)
        {
            stocks.add(buyerBL.getProductStock(product));
        }
        
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("buy", buyer);
        modelMap.addAttribute("sellerProducts", sellersProducts);
        modelMap.addAttribute("stocks", stocks);
        return "buyproducts";
    }
    
    @RequestMapping(value = "/buyer", params={"Back"}, method = RequestMethod.GET)
    public String handleBackButtonSellers(@ModelAttribute("buy") Buyer buyer,
                                          @ModelAttribute("billnumber") Integer randomNumber,
                                          ModelMap modelMap)
    {
        modelMap.addAttribute("billnumber", randomNumber);
        modelMap.addAttribute("buy", buyer);
        return "buyer";
    }
}
