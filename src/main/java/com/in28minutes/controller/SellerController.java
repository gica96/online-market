package com.in28minutes.controller;

import com.in28minutes.bl.SellerBL;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import com.in28minutes.strategy.Creator;
import com.in28minutes.strategy.IComparator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Controller
@SessionAttributes({"sell", "stocks", "productsToAdd"})
public class SellerController {
    
    @Autowired
    SellerBL sellerBL;

    @RequestMapping(value="/seller", method = RequestMethod.GET)
    public String showSellerPage(@ModelAttribute("sell") Seller seller,
                                 @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", stocks);
        return "seller";
    }
   
    @RequestMapping(value ="/seller", params = {"View your products!"}, method = RequestMethod.GET)
    public String showSellerProducts(@ModelAttribute("sell") Seller seller,
                                     @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                     ModelMap modelMap)
    {
        List<Integer> newStocks = new ArrayList<Integer>();
        sellerBL.loadProducts(seller);
        for(Product product : seller.getProducts())
        {
            newStocks.add(sellerBL.getProductStock(product));
        }
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", newStocks);
        return "viewproducts";
    }
    
    @RequestMapping(value = "/seller", params = {"Update products!"}, method = RequestMethod.GET)
    public String showUpdatePage(@ModelAttribute("sell") Seller seller,
                                 @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                 ModelMap modelMap)
    {
        List<Integer> newStocks = new ArrayList<Integer>();
        sellerBL.loadProducts(seller);
        for(Product product : seller.getProducts())
        {
            newStocks.add(sellerBL.getProductStock(product));
        }
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", newStocks);
        return "updateproducts";
    }
    
    @RequestMapping(value = "/seller", params = {"Add products!"}, method = RequestMethod.GET)
    public String showAddProductPage(@ModelAttribute("sell") Seller seller,
                                     ModelMap modelMap)
    {
        Random rn = new Random();
        int zeroOrOne = rn.nextInt(1-0 + 1) + 0;

        List<Product> productsToAdd = new ArrayList<Product>();
        productsToAdd.add(new Product("Haine", "Tricou OM", 90.0));
        productsToAdd.add(new Product("Instrumente Muzicale", "Chitara Electrica", 550.50));
        productsToAdd.add(new Product("Electronice", "Tastatura SteelSeries Apex 350", 400.0));
        productsToAdd.add(new Product("Rachete Tenis", "Wilson BLX 98", 750.0));

        Creator creator = new Creator();
        IComparator iComparator = creator.chooseStrategy(zeroOrOne);
        iComparator.compareProducts(productsToAdd);
        
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("productsToAdd", productsToAdd);
        
        return "addproducts";
    }
    
    @RequestMapping(value = "/seller", params = {"Check notifications!"}, method = RequestMethod.GET)
    public String showNotifications(@ModelAttribute("sell") Seller seller,
                                    ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        return "shownotifications";
    }
}