package com.in28minutes.controller;

import com.in28minutes.model.Seller;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;

@Controller
@SessionAttributes({"sell", "stocks"})
@RequestMapping("/viewproducts")
public class ViewProductsController {
    
    @RequestMapping(value = "/viewproducts", method = RequestMethod.GET)
    public String showSellerProductsPage(@ModelAttribute("sell") Seller seller,
                                   @ModelAttribute("stocks")ArrayList<Integer> stocks,
                                   ModelMap modelMap)
    {
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", stocks);
        return "viewproducts";
    }
    
    @RequestMapping(value = "/seller", params = {"Back"}, method = RequestMethod.GET)
    public String handleBackButtonSellerProducts()
    {
        return "seller";
    }
}
