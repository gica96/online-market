package com.in28minutes.controller;

import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import java.util.ArrayList;

@Controller
@SessionAttributes({"payedorders", "payedproducts"})
@RequestMapping("/vieworders")
public class ViewOrdersController {

    @RequestMapping(value="/vieworders", method = RequestMethod.GET)
    public String showOrdersPage(@ModelAttribute ("payedproducts") ArrayList<Product> products, 
                                 @ModelAttribute("payedorders") ArrayList<MyOrder> orders, 
                                 ModelMap modelMap)
    {
        modelMap.addAttribute("payedproducts", products);
        modelMap.addAttribute("payedorders",orders);
        return "vieworders";
    }

    @RequestMapping(value = "/buyer", params={"Back"}, method = RequestMethod.GET)
    public String handleBackButtonOrders()
    {
        return "buyer";
    }
}
