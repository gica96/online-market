package com.in28minutes.controller;

import com.in28minutes.bl.SellerBL;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@SessionAttributes({"sell", "stocks"})
public class LoginSellerController {
    
    @Autowired
    SellerBL sellerBL;
    
    @RequestMapping(value = "/loginseller", method = RequestMethod.GET)
    public String showLoginSeller(ModelMap modelMap)
    {
        Seller seller = new Seller();
        List<Integer> stocks = new ArrayList<Integer>();
        modelMap.addAttribute("sell", seller);
        modelMap.addAttribute("stocks", stocks);
        return "loginseller";
    }
    
    @RequestMapping(value = "/loginseller", method = RequestMethod.POST)
    public String handleLoginSeller(@ModelAttribute("sell") Seller seller,
                                    @ModelAttribute("stocks") ArrayList<Integer> stocks,
                                    @RequestParam String name,
                                    @RequestParam String password,
                                    ModelMap modelMap)
    {
        Seller newSeller = sellerBL.getSellerByUsernameAndPassword(name, password);
        List<Integer> newStocks = new ArrayList<Integer>();
        if(newSeller != null)
        {
            sellerBL.loadProducts(seller);
            for(Product product : newSeller.getProducts())
            {
                newStocks.add(sellerBL.getProductStock(product));
            }
            modelMap.addAttribute("sell", newSeller);
            modelMap.addAttribute("stocks", newStocks);
            return "seller";
        }
        else 
        {
            modelMap.put("errorMessage", "Acest seller nu exista!");
            return "loginseller";
        }
    }
    
    
}
