package com.in28minutes.IObserver;

import com.in28minutes.bl.BuyerBL;
import com.in28minutes.model.Buyer;
import com.in28minutes.model.Product;

public interface IObserver {
    
    void update(Product product, Buyer buyer, int quantity, BuyerBL buyerBL);
}
