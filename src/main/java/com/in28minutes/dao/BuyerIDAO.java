package com.in28minutes.dao;

import com.in28minutes.model.Buyer;
import com.in28minutes.model.MyOrder;
import com.in28minutes.model.Product;
import com.in28minutes.model.Seller;

import java.util.List;

public interface BuyerIDAO {
    
    Buyer findBuyerByUsernameAndPassword(String username, String password);
    void loadOrders(Buyer buyer);
    Product getProductByOrder(MyOrder order);
    List<Seller> getAllSellers();
    List<Product> getProductsBySeller(Seller seller);
    Seller findSellerByUsername(String username);
    int getProductStock(Product product);
    Product getProductByName(String name);
    void updateProductStock(Product product, int stock);
    void addToCart(MyOrder order);
    void setIsPayed(MyOrder order);
    void updateSellerMoney(int sellerId, double sum);
    void updateBuyerMoney(Buyer buyer, double sum);
    Seller findSellerById(int id);
    void updateSellerNotifications(Seller seller, String newNotification);
}
