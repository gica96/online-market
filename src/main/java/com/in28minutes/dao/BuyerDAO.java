package com.in28minutes.dao;

import com.in28minutes.connection.HBConnection;
import com.in28minutes.model.*;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

public class BuyerDAO implements BuyerIDAO {


    public Buyer findBuyerByUsernameAndPassword(String username, String password) 
    {
        Buyer buyer = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "from Buyer where username = :username and password = :password";
            Query query = sessionObj.createQuery(queryString);
            query.setString("username", username);
            query.setString("password", password);
            buyer = (Buyer)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("1st Mistake");
            ex.printStackTrace();
        }
        return buyer;
    }
    
    public void loadOrders(Buyer buyer)
    {
        buyer.renewOrders();
        int idBuyer = buyer.getIdBuyer();
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM MyOrder where idBuyer_order = :idBuyer_order";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBuyer_order", idBuyer);
            List<MyOrder> list = query.list();
            for(MyOrder myOrder : list)
            {
                buyer.addOrder(myOrder);
            }
        }
        catch(Exception ex)
        {
            System.out.println("2nd Mistake");
            ex.printStackTrace();
        }
    }
    
    public Product getProductByOrder(MyOrder order)
    {
        int idProduct = order.getIdProduct();
        Product product = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Product where idProduct = :idProduct";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idProduct", idProduct);
            product = (Product)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("3rd Mistake");
            ex.printStackTrace();
        }
        return product;
    }
    
    public List<Seller> getAllSellers() {
        List<Seller> sellers = new ArrayList<Seller>();
        try {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Seller";
            Query query = sessionObj.createQuery(queryString);
            sellers = query.list();
        } catch (Exception ex) {
            System.out.println("4th Mistake");
            ex.printStackTrace();
        }
        return sellers;
    }
    
    public List<Product> getProductsBySeller(Seller seller)
    {
        List<Product> products = new ArrayList<Product>();
        try {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Product where idSeller_product = :idSeller_product";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idSeller_product", seller.getIdSeller());
            products = query.list();
        } catch (Exception ex) {
            System.out.println("5th Mistake");
            ex.printStackTrace();
        }
        return products;
    }
    
    public Seller findSellerByUsername(String username)
    {
        Seller seller = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Seller where username = :username";
            Query query = sessionObj.createQuery(queryString);
            query.setString("username", username);
            seller = (Seller)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("6th Mistake");
            ex.printStackTrace();
        }
        return seller;
    }
    
    public int getProductStock(Product product)
    {
        int stock = -1;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM ProductStock where idProduct_stock = :idProduct_stock";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idProduct_stock", product.getIdProduct());
            ProductStock productStock = (ProductStock)query.uniqueResult();
            stock = productStock.getStock();
        }
        catch(Exception ex)
        {
            System.out.println("7th Mistake");
            ex.printStackTrace();
        }
        return stock;
    }
    
    public Product getProductByName(String name)
    {
        Product product = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Product where name = :name";
            Query query = sessionObj.createQuery(queryString);
            query.setString("name", name);
            product = (Product)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("8th Mistake");
            ex.printStackTrace();
        }
        return product;
    }
    
    public void updateProductStock(Product product, int stock)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM ProductStock where idProduct_stock = :idProduct_stock";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idProduct_stock", product.getIdProduct());
            ProductStock newProduct = (ProductStock)query.uniqueResult();
            newProduct.setStock(stock);
            sessionObj.update(newProduct);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("9th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void addToCart(MyOrder order)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            sessionObj.save(order);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("10th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void setIsPayed(MyOrder order)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM MyOrder where idOrder = :idOrder";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idOrder", order.getIdOrder());
            MyOrder newOrder = (MyOrder)query.uniqueResult();
            newOrder.setIsPayed(1);
            sessionObj.update(newOrder);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("11th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void updateSellerMoney(int sellerId, double sum)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Seller where idSeller = :idSeller";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idSeller", sellerId);
            Seller seller = (Seller)query.uniqueResult();
            double oldSum = seller.getMoney();
            double newSum = oldSum + sum;
            seller.setMoney(newSum);
            sessionObj.update(seller);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("12th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void updateBuyerMoney(Buyer buyer, double sum)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Buyer where idBuyer = :idBuyer";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idBuyer", buyer.getIdBuyer());
            Buyer newBuyer = (Buyer)query.uniqueResult();
            double oldSum = newBuyer.getMoney();
            double newSum = oldSum - sum;
            newBuyer.setMoney(newSum);
            sessionObj.update(newBuyer);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("13th Mistake");
            ex.printStackTrace();
        }
    }

    public Seller findSellerById(int id)
    {
        Seller seller = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Seller where idSeller = :idSeller";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idSeller", id) ;
            seller = (Seller)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("14th Mistake");
            ex.printStackTrace();
        }
        return seller;
    }
    
    public void updateSellerNotifications(Seller seller, String newNotification)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Seller where idSeller = :idSeller";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idSeller", seller.getIdSeller());
            Seller newSeller = (Seller)query.uniqueResult();
            String string = newSeller.getNotifications() + " " + newNotification;
            newSeller.setNotifications(string);
            sessionObj.update(newSeller);
            sessionObj.getTransaction().commit(); 
        }
        catch(Exception ex)
        {
            System.out.println("15th Mistake");
            ex.printStackTrace(); 
        } 
    }
}
