package com.in28minutes.dao;

import com.in28minutes.model.Product;
import com.in28minutes.model.ProductStock;
import com.in28minutes.model.Seller;

public interface SellerIDAO {
    
    Seller findSellerByUsernameAndPassword(String username, String password);
    void loadProducts(Seller seller);
    int getProductStock(Product product);
    Product getProductByName(String name);
    void updateProductStock(Product product, int stock);
    void updateProductPrice(Product product, double newPrice);
    void insertProduct(Product product);
    void insertProductStock(ProductStock productStock);
}
