package com.in28minutes.dao;

import com.in28minutes.connection.HBConnection;
import com.in28minutes.model.Product;
import com.in28minutes.model.ProductStock;
import com.in28minutes.model.Seller;
import org.hibernate.Query;
import org.hibernate.Session;

import java.util.List;

public class SellerDAO implements SellerIDAO {


    public Seller findSellerByUsernameAndPassword(String username, String password) 
    {
        Seller seller = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "from Seller where username = :username and password = :password";
            Query query = sessionObj.createQuery(queryString);
            query.setString("username", username);
            query.setString("password", password);
            seller = (Seller)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("1st Mistake");
            ex.printStackTrace();
        }
        return seller;
    }
    
    public void loadProducts(Seller seller)
    {
        seller.renewProducts();
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Product where idSeller_product = :idSeller_product";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idSeller_product", seller.getIdSeller());
            List<Product> list = query.list();
            for(Product product : list)
            {
                seller.addProduct(product);
            }
        }
        catch(Exception ex)
        {
            System.out.println("2nd Mistake");
            ex.printStackTrace();
        }
    }
    
    public int getProductStock(Product product)
    {
        int stockProduct = -1;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM ProductStock where idProduct_stock = :idProduct_stock";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idProduct_stock", product.getIdProduct());
            ProductStock productStock = (ProductStock)query.uniqueResult();
            stockProduct = productStock.getStock();
        }
        catch(Exception ex)
        {
            System.out.println("3rd Mistake");
            ex.printStackTrace();
        }
        return stockProduct;
    }

    public Product getProductByName(String name)
    {
        Product product = null;
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Product where name = :name";
            Query query = sessionObj.createQuery(queryString);
            query.setString("name", name);
            product = (Product)query.uniqueResult();
        }
        catch(Exception ex)
        {
            System.out.println("4th Mistake");
            ex.printStackTrace();
        }
        return product;
    }

    public void updateProductStock(Product product, int stock)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM ProductStock where idProduct_stock = :idProduct_stock";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idProduct_stock", product.getIdProduct());
            ProductStock newProduct = (ProductStock)query.uniqueResult();
            newProduct.setStock(stock);
            sessionObj.update(newProduct);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("5th Mistake");
            ex.printStackTrace();
        }
    }

    public void updateProductPrice(Product product, double newPrice)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            String queryString = "FROM Product where idProduct = :idProduct";
            Query query = sessionObj.createQuery(queryString);
            query.setInteger("idProduct", product.getIdProduct());
            Product newProduct = (Product)query.uniqueResult();
            newProduct.setPrice(newPrice);
            sessionObj.update(newProduct);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("6th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void insertProduct(Product product)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            sessionObj.save(product);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("7th Mistake");
            ex.printStackTrace();
        }
    }
    
    public void insertProductStock(ProductStock productStock)
    {
        try
        {
            Session sessionObj = HBConnection.buildSessionFactory().openSession();
            sessionObj.beginTransaction();
            sessionObj.save(productStock);
            sessionObj.getTransaction().commit();
        }
        catch(Exception ex)
        {
            System.out.println("8th Mistake");
            ex.printStackTrace();
        }
    }
}
