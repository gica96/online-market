<%--
  Created by IntelliJ IDEA.
  User: George
  Date: 5/13/2018
  Time: 6:03 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>View products</title>
</head>
<body>
<p>Aici va puteti vedea toate produsele detinute momentan!</p>

<c:forEach items="${sell.products}" var="products" varStatus="status">
    <br>
    <b>Product ID: </b>
    <c:out value="${products.idProduct}"/>
    <br>
    <b>Categoria: </b>
    <c:out value="${products.category}"/>
    <br>
    <b>Numele produsului: </b>
    <c:out value="${products.name}"/>
    <br>
    <b>Pretul produsului: </b>
    <c:out value="${products.price}"/>
    <br>
    <b>Stock: </b>
    <c:out value="${stocks[status.index]}"/>
    <br>
</c:forEach>
<br>
<form action="/viewproducts/seller" method="GET">
    <input name="Back" type="submit" value="Back">
</form>

</body>
</html>
