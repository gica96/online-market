<%--
  Created by IntelliJ IDEA.
  User: Denisa
  Date: 5/1/2018
  Time: 8:47 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>View Cart</title>
</head>
<body>
<p>Aici va puteti vedea intreg cosul de cumparaturi!</p>

<c:forEach items="${cartorders}" var="orders">
    <br>
    <b>Bill Number: </b>
    <c:out value="${orders.billNumber}"/>
    <br>
    <b>Este platita: </b>
    <c:out value="${orders.isPayed}"/>
    <br>
    <b>Data efectuarii: </b>
    <c:out value="${orders.date}"/>
    <br>
    <b>Cantitatea cumparata: </b>
    <c:out value="${orders.quantity}"/>
    <br>
    <b>Product ID: </b>
    <c:out value="${orders.idProduct}"/>
    <br>
</c:forEach>
<p>Mai jos puteti vedea produsele din cosul de cumparaturi! <br> </p>
<c:forEach items="${cartproducts}" var="product">
    <br>
    <b>Product ID: </b>
    <c:out value="${product.idProduct}"/>
    <br>
    <b>Categoria: </b>
    <c:out value="${product.category}"/>
    <br>
    <b>Numele produsului: </b>
    <c:out value="${product.name}"/>
    <br>
    <b>Pretul produsului: </b>
    <c:out value="${product.price}"/>
    <br>
    <b>ID Seller: </b>
    <c:out value="${product.idSeller}"/>
    <br>
</c:forEach>
<br>

<p><span style = "color:red"> ${errorMessage}</span></p>
<form action="/viewcart/viewcart" method="POST">
    Numarul facturii: <input name="billNumber" type="text"/>
    <input name="Pay bill!"  type="submit" value="Pay bill!"/>
</form>

<form action="/viewcart/buyer" method="POST">
    <input name="Back" type="submit" value="Back">
</form>

</body>

</html>
