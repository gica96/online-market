<%--
  Created by IntelliJ IDEA.
  User: George
  Date: 5/13/2018
  Time: 8:45 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>Update products</title>
</head>
<body>
<p>Aici puteti sa updatati produsele (cantitatea si pretul)!</p>
<b>Lista de produse este disponibila mai jos!</b>
<br>
<c:forEach items="${sell.products}" var="products" varStatus="status">
    <br>
    <b>Product ID: </b>
    <c:out value="${products.idProduct}"/>
    <br>
    <b>Categoria: </b>
    <c:out value="${products.category}"/>
    <br>
    <b>Numele produsului: </b>
    <c:out value="${products.name}"/>
    <br>
    <b>Pretul produsului: </b>
    <c:out value="${products.price}"/>
    <br>
    <b>Stock: </b>
    <c:out value="${stocks[status.index]}"/>
    <br>
</c:forEach>

<p><span style = "color:red"> ${errorMessage}</span></p>
<form action="/updateproducts/updateproducts" method="POST">
    Numele produsului: <input name="productName" type="text"/>
    Noul stock: <input name="quantity" type="text"/>
    <input name="Set stock!"  type="submit" value="Set stock!"/>
</form>

<form action="/updateproducts/updateproducts" method="POST">
    Numele produsului: <input name="productName" type="text"/>
    <input name="Do discount!"  type="submit" value="Do discount!"/>
</form>

<br>
<form action="/updateproducts/seller" method="GET">
    <input name="Back" type="submit" value="Back">
</form>

</body>
</html>
